<!DOCTYPE HTML>

<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>

    <?php
    require_once "templates.php";
    require_once 'user_manager.php';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST["reg_pwd"] === $_POST["reg_pwd_conf"]) {
            $user_manager = new UserManager("dat/users.txt");
            $user_manager->add_from_array($_POST);
            header("Location: login.php");
        } else {
            header("Location: register.php");
        }

    } else {
        render_registration_form();
    }
    ?>

    </body>
</html>
