<?php

require_once 'user.php';

class UserManager
{
    public $users;
    public $path = "";


    public function UserManager($path) {
        if ($path !== "") {
            $this->read_from_file($path);
        } else {
            $this->users = array();
        }
    }

    public function add_from_array($array, $append=true) {
        $this->read_from_file();

        $first_name = $array["reg_first_name"];
        $last_name = $array["reg_last_name"];
        $gender = $array["reg_gender"];
        $email = $array["reg_email"];
        $password = $array["reg_pwd"];
        $id = $this->free_id();

        $user = new User($email, $first_name, $gender, $id, $last_name, md5($password));
        $this->users[$id]= $user;

        if ($append) $this->append_to_file($user);

    }

    private function free_id() {
        $used_ids = array();

        foreach($this->users as $user) {
            $used_ids[]= $user->id;
        }

        print_r($used_ids);
        $id = 0;
        while (in_array($id, $used_ids)) {
            $id++;
        }

        return $id;
    }

    public function read_from_file($path="") {
        $this->users = array();

        if ($path === "") {
            if ($this->path === "") {
                return;
            }
        } else {
            error_log("Setting this->path to: $this->path");
            $this->path = $path;
        }

        if (file_exists($this->path)) {
            $users_array = file($this->path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

            foreach($users_array as $user_array) {
                list($id, $email, $first_name, $last_name, $gender, $pwd_hash) = explode("\t", $user_array);
                $this->users[]= new User($email, $first_name, $gender, $id, $last_name, $pwd_hash);
            }
        }
    }

    public function append_to_file($user, $path="") {

        if ($path === "") {
            if ($this->path === "") return;
        } else {
            $this->path = $path;
        }

        $fp = fopen($this->path, "a");
        $x = "$user->id\t$user->email\t$user->first_name\t$user->last_name\t$user->gender\t$user->pwd_hash\n";
        fputs($fp, $x);

        fclose($fp);

    }

    public function write_to_file($path="") {

        if ($path === "") {
            if ($this->path === "") return;
        } else {
            $this->path = $path;
        }

        $fp = fopen($this->path, "w");

        foreach ($this->users as $user) {
            fputs($fp, "$user->id\t$user->email\t$user->first_name\t$user->last_name\t$user->gender\t$user->pwd_hash\n");;
        }

        fclose($fp);

    }

    public function find_by_email($email) {
        $results = array();

        foreach ($this->users as $user) {
            if ($user->email === $email) $results[]= $user;
        }

        return $results;
    }

    public function find_by_id($id) {
        return $this->users[$id];
    }

    public function find($id) {
        return $this->find_by_id($id);
    }
}
