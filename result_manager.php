<?php

// Todo: Allow an admin user to view/delete the questions somewhere.

require 'result.php';

class ResultManager {
    // Todo: Wouldn't it be nice to be able to append to one's data? That is, to answer yet unanswered questions?
    public $path;
    public $results = array();

    public function ResultManager($path) {
        $this->path = $path;
    }

    public function add_from_post($array, $append=true) {
        $radio_button_values = array();

        foreach($array as $key => $value) {
            // Check if the POST value represents a choice made on one of the radioboxes. (The radioboxes are named q_<id of question>).
            if (preg_match('/^q_[0-9]+$/', $key) === 1) {
                $question_id = str_replace("q_", "", $key);
                echo "KEY: $key, Value: $value <br>";
                $radio_button_values[$question_id] = $value;
            }
        }

//        $person_male = strip_tags($array["required_nl_Male"]);
//        $person_first_name = strip_tags($array["required_nl_Firstname"]);
//        $person_last_name = strip_tags($array["required_nl_Lastname"]);
//        $person_email = strip_tags($array["required_nl_Email"]);
        $person_user_id = $array["required_nl_user_id"];

        $id = $this->free_id();

        $result = new Result($id, $person_user_id, $radio_button_values);
        $this->results[$result->id]= $result;


        if ($append) {
            $this->append_to_file($result);
        }

        return $result;
    }

    public function write_to_file() {
        $fp = fopen($this->path, "w");

        foreach ($this->results as $result) {
            fputs($fp, "$result->id");

            foreach($result->answers as $key => $value) {
                fputs($fp, "\t$key,$value");
            }

            fputs($fp, "\n");
        }

        fclose($fp);
    }

    public function append_to_file($result) {
        $fp = fopen($this->path, "a");

        fputs($fp, "$result->id\t$result->user_id");

        foreach($result->answers as $key => $value) {
            fputs($fp, "\t$key,$value");
        }

        fputs($fp, "\n");

        fclose($fp);

    }

    public function read_from_file() {

        // Clearing the array where the answers are stored.
        $this->results = array();

        if (file_exists($this->path)) {
            // Reading the lines from the file, if it exists.
            $lines = file($this->path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

            foreach ($lines as $line) {
                // Iterating over each line. A line might look like this:
                //0	Vorname	Nachname	e@mai.l	0,1	1,2	2,2	3,3	4,4	8,5
                $answers = array();

                // Tuple-unpacking, or at least the PHP equivalent of it. explode() will split on the separator, returning an array.
                // Example: ["0", "Vorname", "Nachname", "e@mai.l", "0,1", "1,2", "2,2", "3,3,", "4,4,", "8,5"]
                $array = explode("\t", $line);
                // Instead of doing $gender = $array[1]; $first_name = $array[2] ... we'll do this.
                list($result_id, $user_id, $gender, $first_name, $name, $email) = $array;
                // Since PHP doesn't have a splat operator, we'll have to do some slicing. This'll grab all the elements after
                // the fifth one, and will store them in $id_values.
                $id_values = array_slice($array, 2);

                foreach ($id_values as $id_value) {
                    // Each item of the $id_values array will look something like "1,4".
                    // The value before the comma is the ID of the question, the value after the comma indicates which answer was selected.
                    list($question_id, $value) = explode(",", $id_value);
                    $answers[$question_id] = $value;
                }

                // Creating a new instance of Answer, with those values.
                $this->results[$result_id] = $result = new Result($result_id, $user_id, $answers);
            }
        }


    }

    public function compare_result_with_others($result) {
        $accordance = array();

        foreach($this->results as $result_to_compare_with) {
            if ($result->id !== $result_to_compare_with->id) {
                $accordance[$result_to_compare_with->id] = $this->compare_results($result, $result_to_compare_with);
            } else {
            }
        }

        return $accordance;
    }

    public function compare_results($result1, $result2) {
        $differences_accordance_map = array(0 => 100,
                                            1 => 75,
                                            2 => 50,
                                            3 => 25,
                                            4 => 0);

        $accordance = array();

        $answers1 = $result1->answers;
        $answers2 = $result2->answers;

        foreach($answers1 as $question_id => $answer) {
            // Checking if the question was answered in the second result, too.
            if (isset($answers2[$question_id])) {
                $difference = abs($answers1[$question_id] - $answers2[$question_id]);
                $accordance[$question_id] = $differences_accordance_map[$difference];
            }
        }

        // With some bad luck, the two results won't have any questions in common. Wouldn't want a DivisionByZero error, would we. ;)
        // Todo: Would be prettier to show something like "Not enough data".
        if (count($accordance) === 0) {
            return 0;
        } else {
            return array_sum($accordance) / count($accordance);
        }
    }

    private function free_id() {
        $taken_ids = array();

        foreach($this->results as $result) {
            $taken_ids[]= $result->id;
        }

        $free_id = 0;
        while (in_array($free_id, $taken_ids)) {
            $free_id++;
        }

        return $free_id;

    }

    public function find_by_id($id) {
        return $this->results[$id];
    }

    public function find($id) {
        return $this->find_by_id($id);
    }

}