<?php
require_once 'result_manager.php';
require_once 'helper.php';
require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_radar.php');

session_start();

$current_user = current_user();

if (!$current_user) {
    redirect_to("login.php");
}

$result_manager = new ResultManager("dat/antworten.txt");
$result_manager->read_from_file();

# Todo: Would be nice if it were to work for more than two results.
list($rid1, $rid2) = explode(",", $_GET["result_ids"]);
$result1 = $result_manager->find($rid1);
$result2 = $result_manager->find($rid2);

$plot_titles = array();
$plot_data_1 = array();
$plot_data_2 = array();

# Todo: Doing this here as well as in ResultManager#compare, a helper function would cut down on the redundancy.
foreach ($result1->answers as $question_id => $value) {
    if (isset($result2->answers[$question_id])) {
        $plot_titles[] = "Question $question_id";
        $plot_data_1[] = $value;
        $plot_data_2[] = $result2->answers[$question_id];
    }
}

// Create the basic rtadar graph
$graph = new RadarGraph(600,400);

// Set background color and shadow
$graph->SetColor("white");
$graph->SetShadow();

// Position the graph
$graph->SetCenter(0.4,0.5);

// Setup the axis formatting
$graph->axis->SetFont(FF_FONT1,FS_BOLD);
$graph->axis->SetWeight(2);

// Setup the grid lines
$graph->grid->SetLineStyle("longdashed");
$graph->grid->SetColor("navy");
$graph->grid->Show();
//$graph->HideTickMarks();

// Setup graph titles
$graph->title->Set("Umfrageresultate mit jpgraph/RadarPlot");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->SetTitles($plot_titles);

// Create the first radar plot
$plot = new RadarPlot($plot_data_1);
$plot->SetLegend($result1->user()->full_name());
$plot->SetColor("red","lightred");
$plot->SetFill(false);
$plot->SetLineWeight(2);

// Create the second radar plot
$plot2 = new RadarPlot($plot_data_2);
$plot2->SetLegend($result2->user()->full_name());
$plot2->SetColor("blue","lightblue");

// Add the plots to the graph
$graph->Add($plot2);
$graph->Add($plot);

// And output the graph
$graph->Stroke();

