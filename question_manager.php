<?php

// Todo: Allow an admin user to edit the questions somewhere.

require 'question.php';

class QuestionManager
{
    public $questions = array();
    
    public function QuestionManager() {

    }

    public function read_from_file($path) {

        if (file_exists($path)) {
            // Grabbing all the lines from the specified file, storing them in an array.
            $question_strings = File($path);

            foreach ($question_strings as $question_string) {
                // See ResultManager->read_from_file for a detailed explanation.
                list($id, $question) = explode("§", $question_string);
                $this->questions[$id]= new Question($question, $id);
            }
        }

    }

    public function random_questions($amount) {
        if ($amount > count($this->questions)) {
            $amount = count($this->questions);
        }

        // Fetching $amount random keys.
        $random_keys = array_rand($this->questions, $amount);
        $random_values = array();

        // Adding each corresponding value to $random_values and returning that one.
        foreach ($random_keys as $random_key) {
            $random_values[]= $this->questions[$random_key];
        }

        return $random_values;
    }

    public function find_by_id($id) {
        return $this->questions[$id];
    }

    public function find($id) {
        return $this->find_by_id($id);
    }
}
