<?php

require_once 'user_manager.php';

/**
 * Returns whether a user is signed in.
 *
 * @return Boolean -> true if there is a user signed in, false if not.
 *
 */
function is_logged_in() {
    return (array_key_exists("signed_in", $_SESSION) && $_SESSION["signed_in"]);
}

/**
 * Redirects the user to $target.
 *
 * @param  $target -> The page to which to redirect the user to. E.g. 'index.php'.
 *
 */
function redirect_to($target) {
    header("Location: $target");
    exit;
}

/**
 * Returns the current user if he is signed in, respectively 'false' if no one is signed in.
 *
 * This allows for constructs like '$user = current_user(); if (!user) { redirect_to("login.php"); }'
 *
 * @return User|Boolean -> Current user or boolean.
 *
 */
function current_user() {
    if (is_logged_in()) {
        $um = new UserManager("dat/users.txt");
        $id = $_SESSION["user_id"];
        return $um->find_by_id($id);
    } else {
        return false;
    }

}