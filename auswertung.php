<?php
require_once 'result_manager.php';
require_once 'user_manager.php';
require_once 'user.php';
require_once 'templates.php';
require_once 'helper.php';

session_start();

$current_user = current_user();

if (!$current_user) {
    redirect_to("login.php");
}


$result_manager = new ResultManager("dat/antworten.txt");
$result_manager->read_from_file();
$result = $result_manager->find($_GET["result_id"]);

$accordance = $result_manager->compare_result_with_others($result);
arsort($accordance);
?>

<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
    <head>
        <title></title>
        <meta name="author" content="jacob">
        <meta charset="UTF-8">
        <meta name="generator" content="Ulli Meybohms HTML EDITOR">
        <link rel="stylesheet" href="umfrage.css" type="text/css">
    </head>

    <body text="#000000" bgcolor="#FFFFFF" link="#FF0000" alink="#FF0000" vlink="#FF0000">
        <SPAN>
            <TABLE cellSpacing=0 cellPadding=0 width=578 border=0>
                <TBODY>
                    <TR align=left>
                        <TD class=norm vAlign=top noWrap></TD>
                            <BR><BR>
                            <SPAN class="title">Auswertung PartnerWahl</SPAN>
                            <BR><BR>
                        </TD>
                    </TR>
                </TBODY>
            </TABLE>
            <TABLE cellSpacing=0 cellPadding=0 width=578 border=0>
                <TBODY>
                    <TR vAlign=top align=left>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif" width=20>
                        </TD>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif" width=200>
                        </TD>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif" width=1>
                        </TD>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif" width=30>
                        </TD>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif" width=100>
                        </TD>
                        <TD class=norm>
                            <IMG height=1 src="empty.gif"
                        width=30>
                        </TD>
                    </TR>

                    <TR class=headerrow vAlign=top align=left>
                        <TD class=norm align=right>Nr&nbsp;</TD>
                        <TD class=norm>Name</TD>
                        <TD class=norm>&nbsp;</TD>
                        <TD class=norm colSpan=2>&Uuml;bereinstimmung</TD>
                        <TD class=norm>Profil</TD>
                    </TR>
                    <TR>
                        <TD class=lineseparator vAlign=top align=left colSpan=7><IMG
                        height=1 src="empty.gif"
                        width=1></TD>
                    </TR>

                    <?php
                    $i = 1;
                    foreach ($accordance as $result_id => $value) {
                        $result2 = $result_manager->results[$result_id];
                        $user = $result2->user();
                        result_entry(round($value, 2), $user, $i, $result->id, $result2->id);
                        $i++;
                    }
                    ?>
                    <tr>
                        <td colspan=5><a href="logout.php">Logout</a></td>
                    </tr>
                </TBODY>
            </TABLE>
        </SPAN>
    </body>
</html>
