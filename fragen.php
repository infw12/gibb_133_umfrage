<?php

require_once 'user_manager.php';
require_once 'user.php';
require_once 'result_manager.php';
require_once 'question_manager.php';
require_once 'templates.php';
require_once "helper.php";

session_start();

// $user === false if none is signed in.
// $user is a User object if someone is signed in.

if (!($user = current_user())) {
    // If the user is not signed in, we'll redirect him to the login page.
    redirect_to("login.php");
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // If the request method is POST, we'll save the submitted results.
    $result_manager = new ResultManager("dat/antworten.txt");
    // Reading existing answers from the storage.
    $result_manager->read_from_file();
    // Adding the new answer, which'll get added to the storage, too.
    $result = $result_manager->add_from_post($_POST, true);

    redirect_to("auswertung.php?result_id=$result->id");
}

// If it's not POST it'll most likely be GET, and we'll proceed with rendering the questionnaire.
$question_manager = new QuestionManager();
$question_manager->read_from_file("dat/fragen.txt");
?>

<!doctype html public "-//W3C//DTD HTML 4.0 //EN">
<html>
	<head>
		<title></title>
		<meta name="author" content="jacob">
        <meta charset="UTF-8">
		<meta name="generator" content="Ulli Meybohms HTML EDITOR">

		<link rel="stylesheet" href="umfrage.css" type="text/css">

		<SCRIPT language=JavaScript>
			<!--
			function checkForm() {
                var errText = '';
                if ( document.candsforvote.required_nl_Firstname.value == '' ) errText += '- Vorname fehlt\n';
                if ( document.candsforvote.required_nl_Lastname.value == '' ) errText += '- Nachname fehlt\n';
                if ( document.candsforvote.required_nl_Email.value == '' ) errText += '- E-Mail fehlt\n';
                if ( errText != '' ) {
                    alert( errText );
                    return false;
                }
			    else {
			        return true;
			    }
			}
			// -->
		</SCRIPT>
	</head>

	<body text="#000000" bgcolor="#FFFFFF" link="#FF0000" alink="#FF0000" vlink="#FF0000">
        <FORM id=candsforvote name=candsforvote action=fragen.php method=post onSubmit="return checkForm()">
            <input type="hidden" id="required_nl_user_id" name="required_nl_user_id" value=<?= $user->id ?> >
            <BR>
            <div class="title">Fragebogen Partnerwahl</div>
            <TABLE cellSpacing=0 cellPadding=0 width=578 border=0>
                <TBODY>
                    <TR align=left>
                        <TR align=left>
                            <TD class=norm vAlign=top noWrap>
                                <IMG height=1 src="empty.gif" width=100>
                            </TD>
                            <TD class=norm vAlign=center width="100%">
                                <IMG height=1 src="empty.gif" width=1>
                            </TD>
                        </TR>
                        <TR align=left>
                            <TD class=norm vAlign=top noWrap>Gender</TD>
                            <TD class=norm width="100%">
                                <input type="text" class=formular id=required_nl_Male maxLength=100 size=30 name=required_nl_Male readonly value = <?= $user->gender_pretty() ?>>
                            </TD>
                        </TR>
                        <TR align=left>
                            <TD class=norm vAlign=top noWrap>First Name</TD>
                            <TD class=norm vAlign=center width="100%">
                                <INPUT class=formular id=required_nl_Firstname maxLength=100 size=30 name=required_nl_Firstname readonly value=<?= $user->first_name ?> >
                            </TD>
                        </TR>
                        <TR align=left>
                            <TD class=norm vAlign=top noWrap>Surname</TD>
                            <TD class=norm vAlign=center width="100%">
                                <INPUT class=formular id=required_nl_Lastname maxLength=100 size=30 name=required_nl_Lastname readonly value=<?= $user->last_name ?> >
                            </TD>
                        </TR>
                        <TR align=left>
                            <TD class=norm vAlign=top noWrap>E-Mail</TD>
                            <TD class=norm vAlign=center width="100%">
                                <INPUT class=formular id=required_nl_Email maxLength=100 size=30 name=required_nl_Email readonly value=<?= $user->email ?> >
                            </TD>
                        </TR>
                </TBODY>
            </TABLE>
            <TABLE cellSpacing=1 cellPadding=1 width=578 border=0>
                <TBODY>
                    <TR class=headerrow vAlign=top align=left>
                        <TD class=norm><B>&nbsp;Fragen</B></TD>
                        <TD class=norm align=middle>
                            <IMG height=1 src="empty.gif" width=45><BR>ja
                        </TD>
                        <TD class=norm align=middle>
                            <IMG height=1 src="empty.gif" width=45><BR>eher<BR>ja
                        </TD>
                        <TD class=norm align=middle>
                            <IMG height=1 src="empty.gif" width=45><BR>egal
                        </TD>
                        <TD class=norm align=middle>
                            <IMG height=1 src="empty.gif" width=45><BR>eher<BR>nein
                        </TD>
                        <TD class=norm align=middle>
                            <IMG height=1 src="empty.gif" width=45><BR>nein
                        </TD>
                    </TR>

                    <?php
                    $i = 1;
                    foreach ($question_manager->random_questions(10) as $question) {
                        render_question_entry($question, $i, 3);
                        $i++;
                    }
                    ?>

                    <tr>
                        <td><input type="submit"></td>
                    </tr>
                    <tr>
                        <td><a href="logout.php">Logout</a></td>
                    </tr>
                </TBODY>
            </TABLE>
        </FORM>
	</body>
</html>
