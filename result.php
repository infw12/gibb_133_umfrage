<?php
// Todo: Store values of $answers array as integers, and NOT as strings. :)
require_once "user_manager.php";

class Result
{
    public $id;
    public $user_id;
//    public $gender;
//    public $first_name;
//    public $name;
//    public $email;
    public $answers = array();

    public static $gender_map = array("0" => "Female", "1" => "Male");

    public function Result($id, $user_id, $answers) {
        $this->id = $id;
        $this->user_id = $user_id;
//        $this->gender = $gender;
//        $this->first_name = $first_name;
//        $this->name = $name;
//        $this->email = $email;
        $this->answers = $answers;
    }

    public function user() {
        $user_manager = new UserManager("dat/users.txt");
        return $user_manager->find_by_id($this->user_id);
    }

    public function pretty($separator="\n") {
        $user = $this->user();
        $gender = self::$gender_map[$user->gender];

        return "Gender: $gender$separator
                First name: $user->first_name$separator
                Surname: $user->name$separator
                E-Mail: $user->email$separator";
    }



}