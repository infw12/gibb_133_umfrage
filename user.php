<?php

class User
{
    public $id;
    public $first_name;
    public $last_name;
    public $gender;
    public $email;
    public $pwd_hash;


    function __construct($email, $first_name, $gender, $id, $last_name, $pwd_hash) {
        $this->email = $email;
        $this->first_name = $first_name;
        $this->gender = $gender;
        $this->id = $id;
        $this->last_name = $last_name;
        $this->pwd_hash = $pwd_hash;
    }

    public function full_name() {
        return "$this->first_name $this->last_name";
    }

    public function is_female() {
        return $this->gender === "female";
    }

    public function is_male() {
        return $this->gender === "male";
    }

    public function gender_pretty() {
        switch ($this->gender) {
            case "male":
                return "Male    ";
                break;
            case "female":
                return "Female";
                break;
        }
    }

    public function update_password($password) {
        $this->pwd_hash = md5($password);
    }

    public function password_match($password) {
        return $this->pwd_hash === md5($password);
    }

    public function pretty() {
        return "$this->first_name $this->last_name ($this->email | $this->id)";
    }


}
