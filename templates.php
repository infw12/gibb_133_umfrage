<?php

function render_login_form() {
    ?>
    <form action="login.php" method="post" name="user_login">
        <table>
            <tbody>
            <tr>
                <td><label for="login_email">E-Mail</label></td>
                <td><input name="login_email" id="login_email" type="email" required></td>
            </tr>
            <tr>
                <td><label for="login_pwd">Password</label></td>
                <td><input name="login_pwd" id="login_pwd" type="password" required></td>
            </tr>
            <tr>
                <td colspan=2><input name="login_submit" id="login_submit" type="submit" required></td>
            </tr>
            <tr>
                <td colspan=2><a href="register.php">No account? Register here.</a></td>
            </tr>
            </tbody>
        </table>
    </form>
<?php
}

function render_registration_form() {
    ?>
    <form action="register.php" method="post" name="user_reg">
        <table>
            <tbody>
            <tr>
                <td><label for="reg_gender">Gender</label></td>
                <td>
                    <select name="reg_gender" id="reg_gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="reg_first_name">First Name</label></td>
                <td><input name="reg_first_name" id="reg_first_name" type="text" required></td>
            </tr>
            <tr>
                <td><label for="reg_last_name">Last Name</label></td>
                <td><input name="reg_last_name" id="reg_last_name" type="text" required></td>
            </tr>
            <tr>
                <td><label for="reg_email">E-Mail</label></td>
                <td><input name="reg_email" id="reg_email" type="email" required></td>
            </tr>
            <tr>
                <td><label for="reg_pwd">Password</label></td>
                <td><input name="reg_pwd" id="reg_pwd" type="password" required></td>
            </tr>
            <tr>
                <td><label for="reg_pwd_conf">Password confirmation</label></td>
                <td><input name="reg_pwd_conf" id="reg_pwd_conf" type="password" required></td>
            </tr>
            <tr>
                <td colspan=2><input name="reg_submit" id="reg_submit" type="submit" required></td>
            </tr>
            </tbody>
        </table>
    </form>
    <?php
}

// Todo: This function could use some overhauling regarding its parameters.
function result_entry($value, $user, $i, $result_id1, $result_id2) {
    ?>
    <TR vAlign=top align=left>
        <TD class=norm align=right><?= $i ?>&nbsp;</TD>
        <TD class=norm>
            <A onclick="myPopupWindow('x.html','portraet', 768, 550, true, false, true); return false;" href= <?= "result_view.php?result_id=$result_id2" ?> >
                <?= $user->full_name() ?>
            </A>
        </TD>

        <TD class=norm>&nbsp;</TD>
        <TD class=norm><?= "$value%" ?>&nbsp;</TD>
        <TD class=norm>
            <TABLE cellSpacing=0 cellPadding=0 width=100 border=0>
                <TBODY>
                
                <TR>
                    <TD class=harmonybar>
                        <IMG height=10 src="empty.gif" width=<?= $value ?>>
                    </TD>
                    <TD class=harmonybarempty>
                        <IMG height=10 src="empty.gif" width=<?= 100 - $value ?>>
                    </TD>
                </TR>
                </TBODY>
            </TABLE>
        </TD>
        <TD class=norm align=middle>
            <A onclick="myPopupWindow('x.html','spider',768,530, true, false, true);" href= <?= "graph.php?result_ids=$result_id1,$result_id2" ?> >
                <IMG alt="Grafisches Spiderdiagramm (smartspider)" src="icon-spider.gif" border=0>
            </A>
        </TD>
    </TR>
    <TR>
        <TD class=lineseparator vAlign=top align=left colSpan=7>
            <IMG height=1 src="empty.gif" width=1>
        </TD>
    </TR>
    <?php
    }

function render_question_entry($question, $i, $value) {
    $value = intval($value);
    ?>
    <tr class=<?= $i % 2 == 0 ? "evenrow" : "oddrow" ?>>
        <td><?= "[$question->id] $question->question" ?></td>
        <td class="questiontable" align=center>
            <input type="radio" value=1 name="<?= "q_$question->id" ?>" <?php if($value === 1) { echo "checked"; }  ?> >
        </td>
        <td class="questiontable" align=center>
            <input type="radio" value=2 name="<?= "q_$question->id" ?>" <?php if($value === 2) { echo "checked"; }  ?> >
        </td>
        <td class="questiontable" align=center>
            <input type="radio" value=3 name="<?= "q_$question->id" ?>" <?php if($value === 3) { echo "checked"; }  ?> >
        </td>
        <td class="questiontable" align=center>
            <input type="radio" value=4 name="<?= "q_$question->id" ?>" <?php if($value === 4) { echo "checked"; }  ?> >
        </td>
        <td class="questiontable" align=center>
            <input type="radio" value=5 name="<?= "q_$question->id" ?>" <?php if($value === 5) { echo "checked"; }  ?> >
        </td>
    </tr>
    <?php
}

?>