<!DOCTYPE HTML>

<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<?php
require_once "templates.php";
require_once 'user_manager.php';
require_once 'user.php';
require_once 'helper.php';

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // If using POST we'll try to sign the user in.
    $user_manager = new UserManager("dat/users.txt");
    $users = $user_manager->find_by_email($_POST["login_email"]);

    // If we found a user with the specified email, we'll proceed.
    if (count($users) === 1) {
        $user = $users[0];
        if ($user->password_match($_POST["login_pwd"])) {//($user->password_match($_POST["login_pwd"])) {
            // If the password matches, we'll sign the user in and redirect to fragen.php
            $_SESSION["signed_in"] = true;
            $_SESSION["user_id"] = $user->id;
            redirect_to("fragen.php");
        } else {
            // If the password does not match, we'll redirect him back to login.php.
            redirect_to("login.php");
        }
    } else {
        // If we did not find a user with the specified email, we'll redirect him back to login.php.
        redirect_to("login.php");
    }

} else {
    // If using GET, we'll render the login form.
    render_login_form();
}
?>

</body>
</html>
